<?php

namespace App\Services;

use App\Traits\ConsumesExternalService;

class BackupMssqlService{

    use ConsumesExternalService;

    public $baseUri;

    public $secret;

    public function __construct(){

        $this->baseUri = config('services.backupmssql.base_uri');
        $this->secret = config('services.backupmssql.secret');
    }

    public function index(){

        return $this->performRequest('GET', '/backupmssql');
    }

    public function indexyear($id){

        return $this->performRequest('GET', "/backupmssql/{$id}");
    }

    public function indexmonth($id, $idm){

        return $this->performRequest('GET', "/backupmssql/{$id}/month/{$idm}");
    }

    public function detail(){

        return $this->performRequest('GET', "/backupmssqldetail");
    }

    public function detailyear($id){

        return $this->performRequest('GET', "/backupmssqldetail/{$id}");
    }

    public function detailmonth($id, $idm){

        return $this->performRequest('GET', "/backupmssqldetail/{$id}/month/{$idm}");
    }

}
