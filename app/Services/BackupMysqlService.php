<?php

namespace App\Services;

use App\Traits\ConsumesExternalService;

class BackupMysqlService{
    
    use ConsumesExternalService;

    public $baseUri;

    public $secret;

    public function __construct(){

        $this->baseUri = config('services.backupmysql.base_uri');
        $this->secret = config('services.backupmysql.secret');
    }

    public function index(){

        return $this->performRequest('GET', '/backupmysql');
    }

    public function indexyear($id){

        return $this->performRequest('GET', "/backupmysql/{$id}");
    }

    public function indexmonth($id, $idm){

        return $this->performRequest('GET', "/backupmysql/{$id}/month/{$idm}");
    }

    public function detail(){

        return $this->performRequest('GET', "/backupmysqldetail");
    }

    public function detailyear($id){

        return $this->performRequest('GET', "/backupmysqldetail/{$id}");
    }

    public function detailmonth($id, $idm){

        return $this->performRequest('GET', "/backupmysqldetail/{$id}/month/{$idm}");
    }

}
