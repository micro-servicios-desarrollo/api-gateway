<?php

return [
    'backupmysql' => [
        'base_uri' => env('BACKUPMYSQL_SERVICE_BASE_URL'),
        'secret' => env('BACKUPMYSQL_SERVICE_SECRET'),
    ],

    'backuppgsql' => [
        'base_uri' => env('BACKUPPGSQL_SERVICE_BASE_URL'),
        'secret' => env('BACKUPPGSQL_SERVICE_SECRET'),
    ],

    'backupmssql' => [
        'base_uri' => env('BACKUPMSSQL_SERVICE_BASE_URL'),
        'secret' => env('BACKUPMSSQL_SERVICE_SECRET'),
    ],
];
