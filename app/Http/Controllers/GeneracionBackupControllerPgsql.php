<?php

namespace App\Http\Controllers;

use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Services\BackupPgsqlService;

class GeneracionBackupControllerPgsql extends Controller
{
    use ApiResponser;

    public $backuppgsqlservice;

    public function __construct(BackupPgsqlService $backuppgsqlservice)
    {
        $this->backuppgsqlservice = $backuppgsqlservice;
    }

    public function index()
    {
        return $this->successResponse($this->backuppgsqlservice->index());
    }

    public function indexyear($id)
    {
        return $this->successResponse($this->backuppgsqlservice->indexyear($id));
    }

    public function indexmonth($id, $idm)
    {
        return $this->successResponse($this->backuppgsqlservice->indexmonth($id, $idm));
    }

    public function detail()
    {
        return $this->successResponse($this->backuppgsqlservice->detail());
    }

    public function detailyear($id)
    {
        return $this->successResponse($this->backuppgsqlservice->detailyear($id));
    }

    public function detailmonth($id, $idm)
    {
        return $this->successResponse($this->backuppgsqlservice->detailmonth($id, $idm));
    }

}
