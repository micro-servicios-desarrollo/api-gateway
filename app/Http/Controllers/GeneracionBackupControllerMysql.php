<?php

namespace App\Http\Controllers;

use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Services\BackupMysqlService;

class GeneracionBackupControllerMysql extends Controller
{
    use ApiResponser;

    public $backupmysqlservice;

    public function __construct(BackupMysqlService $backupmysqlservice)
    {
        $this->backupmysqlservice = $backupmysqlservice;
    }

    public function index()
    {
        return $this->successResponse($this->backupmysqlservice->index());
    }

    public function indexyear($id)
    {
        return $this->successResponse($this->backupmysqlservice->indexyear($id));
    }

    public function indexmonth($id, $idm)
    {
        return $this->successResponse($this->backupmysqlservice->indexmonth($id, $idm));
    }

    public function detail()
    {
        return $this->successResponse($this->backupmysqlservice->detail());
    }

    public function detailyear($id)
    {
        return $this->successResponse($this->backupmysqlservice->detailyear($id));
    }

    public function detailmonth($id, $idm)
    {
        return $this->successResponse($this->backupmysqlservice->detailmonth($id, $idm));
    }

}
