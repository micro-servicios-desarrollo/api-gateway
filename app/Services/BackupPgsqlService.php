<?php

namespace App\Services;

use App\Traits\ConsumesExternalService;

class BackupPgsqlService{
    
    use ConsumesExternalService;

    public $baseUri;

    public $secret;

    public function __construct(){

        $this->baseUri = config('services.backuppgsql.base_uri');
        $this->secret = config('services.backuppgsql.secret');
    }

    public function index(){

        return $this->performRequest('GET', '/backuppgsql');
    }

    public function indexyear($id){

        return $this->performRequest('GET', "/backuppgsql/{$id}");
    }

    public function indexmonth($id, $idm){

        return $this->performRequest('GET', "/backuppgsql/{$id}/month/{$idm}");
    }

    public function detail(){

        return $this->performRequest('GET', "/backuppgsqldetail");
    }

    public function detailyear($id){

        return $this->performRequest('GET', "/backuppgsqldetail/{$id}");
    }

    public function detailmonth($id, $idm){

        return $this->performRequest('GET', "/backuppgsqldetail/{$id}/month/{$idm}");
    }

}
