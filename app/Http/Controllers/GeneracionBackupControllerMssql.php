<?php

namespace App\Http\Controllers;

use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Services\BackupMssqlService;

class GeneracionBackupControllerMssql extends Controller
{
    use ApiResponser;

    public $backupmssqlservice;

    public function __construct(BackupmssqlService $backupmssqlservice)
    {
        $this->backupmssqlservice = $backupmssqlservice;
    }

    public function index()
    {
        return $this->successResponse($this->backupmssqlservice->index());
    }

    public function indexyear($id)
    {
        return $this->successResponse($this->backupmssqlservice->indexyear($id));
    }

    public function indexmonth($id, $idm)
    {
        return $this->successResponse($this->backupmssqlservice->indexmonth($id, $idm));
    }

    public function detail()
    {
        return $this->successResponse($this->backupmssqlservice->detail());
    }

    public function detailyear($id)
    {
        return $this->successResponse($this->backupmssqlservice->detailyear($id));
    }

    public function detailmonth($id, $idm)
    {
        return $this->successResponse($this->backupmssqlservice->detailmonth($id, $idm));
    }

}
