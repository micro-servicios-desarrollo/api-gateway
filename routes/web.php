<?php

//$router->group(['prefix' => 'api/v1'], function () use ($router) {

$router->group(['middleware' => 'client.credentials', 'prefix' => 'api/v1'], function () use ($router) {

	$router->get('/backupmysql', 'GeneracionBackupControllerMysql@index');
	$router->get('/backupmysql/{id}', 'GeneracionBackupControllerMysql@indexyear');
	$router->get('/backupmysql/{id}/month/{idm}', 'GeneracionBackupControllerMysql@indexmonth');
	$router->get('/backupmysqldetail', 'GeneracionBackupControllerMysql@detail');
	$router->get('/backupmysqldetail/{id}', 'GeneracionBackupControllerMysql@detailyear');
	$router->get('/backupmysqldetail/{id}/month/{idm}', 'GeneracionBackupControllerMysql@detailmonth');

	$router->get('/backuppgsql', 'GeneracionBackupControllerPgsql@index');
	$router->get('/backuppgsql/{id}', 'GeneracionBackupControllerPgsql@indexyear');
	$router->get('/backuppgsql/{id}/month/{idm}', 'GeneracionBackupControllerPgsql@indexmonth');
	$router->get('/backuppgsqldetail', 'GeneracionBackupControllerPgsql@detail');
	$router->get('/backuppgsqldetail/{id}', 'GeneracionBackupControllerPgsql@detailyear');
	$router->get('/backuppgsqldetail/{id}/month/{idm}', 'GeneracionBackupControllerPgsql@detailmonth');

	$router->get('/backupmssql', 'GeneracionBackupControllerMssql@index');
	$router->get('/backupmssql/{id}', 'GeneracionBackupControllerMssql@indexyear');
	$router->get('/backupmssql/{id}/month/{idm}', 'GeneracionBackupControllerMssql@indexmonth');
	$router->get('/backupmssqldetail', 'GeneracionBackupControllerMssql@detail');
	$router->get('/backupmssqldetail/{id}', 'GeneracionBackupControllerMssql@detailyear');
	$router->get('/backupmssqldetail/{id}/month/{idm}', 'GeneracionBackupControllerMssql@detailmonth');
});

